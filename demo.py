#测试用最少的代码，分别用以下图表库绘制图表
#建议单独运行每个图表，把不运行的注释掉

#data
x = ['Mon','Tue','Web','Thu','Fri','Sat','Sun']
y = [102,123,123,127,199,165,190]


#matplotlib
# import matplotlib.pyplot as plt
# import numpy as np
# plt.style.use('_mpl-gallery')
# fig, ax = plt.subplots()
# ax.bar(x, y)
# plt.show() #绘制图表，完成后会直接弹出



#seaborn
# import matplotlib.pyplot as plt
# import seaborn as sns
# sns.barplot(x, y )
# plt.show()  #绘制图表，完成后会直接弹出



#bokeh
# from bokeh.io import show
# from bokeh.models import ColumnDataSource
# from bokeh.palettes import Spectral6
# from bokeh.plotting import figure
# source = ColumnDataSource(data=dict(x=x, y=y))
# p = figure(x_range=x, title="给小雨三连")
# p.vbar(x='x', top='y', width=0.9, source=source)
# show(p) #绘制图表，会在浏览器中弹出


# #pyecharts
# from pyecharts.charts import Bar
# bar = Bar()
# bar.add_xaxis(xaxis_data=x)
# bar.add_yaxis(series_name='Weekly Sales', y_axis = y)
# bar.render() #绘制图表,会在代码目录下生成一个render.html文件，图表不会直接弹出来


# plotly
# import plotly.express as px
# fig = px.bar( x=x, y=y)
# fig.show() #绘制图表，会在浏览器中直接运行